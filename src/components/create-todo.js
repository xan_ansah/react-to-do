import React from 'react';

export default class CreateTodo extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			error: null
		};
	}

	renderError() {
		if (!this.state.error) { return null; }

		return <div style={{ color : 'red'}}>{this.state.error}</div>;
	}

	render() {
		return (
			<form onSubmit={this.handleCreate.bind(this)}>
				<input placeholder="What do I need to do?" type="text" ref="createInput"/>
				<button>Create</button>
				{this.renderError()}
			</form>
		);
	}

	handleCreate(event) {
		event.preventDefault();

		const createInput = this.refs.createInput;
		const task = createInput.value;
		const validateInput = this.validateInput(task);
		
		if(validateInput) {
			this.setState({ error: validateInput });
			return;
		} 
		
		this.setState({ error: null });
		this.props.createTask(task);
		this.refs.createInput.value = '';
	}

	validateInput(task) {
		if (!task) {
			return 'Please enter a new task.'
		} else if (_.find(this.props.todos, todos => todos.task === task)) {
			return 'This task already exists. Please enter a new task.'
		} else {
			return null;
		}
	}
} 